# DL Models

I thought it would be cool to see if I could integrate all these models I have created over the years into Roblox.
Thus, this project has been created to host a website with some models I had managed to make work with Roblox.

This publicly available project contains the notebooks where the models were trained. You can view the code of them for those who are interested!